/**
 * Main JavaScript
 * Baby Spa Rebuild
 *
 * Copyright (c) 2015. by Way2CU, http://way2cu.com
 * Authors: Mladen Mijatov, Anusic Vladimir
 */

// create or use existing site scope
var Site = Site || {};

// make sure variable cache exists
Site.variable_cache = Site.variable_cache || {};


/**
 * Check if site is being displayed on mobile.
 * @return boolean
 */
Site.is_mobile = function() {
	var result = false;

	// check for cached value
	if ('mobile_version' in Site.variable_cache) {
		result = Site.variable_cache['mobile_version'];

	} else {
		// detect if site is mobile
		var elements = document.getElementsByName('viewport');

		// check all tags and find `meta`
		for (var i=0, count=elements.length; i<count; i++) {
			var tag = elements[i];

			if (tag.tagName == 'META') {
				result = true;
				break;
			}
		}

		// cache value so next time we are faster
		Site.variable_cache['mobile_version'] = result;
	}

	return result;
};

/**
 * Handle creating and opening YouTube videos
 * @param object event
 */
Site.handleVideoOpen = function(event) {

	var iframe = document.createElement('iframe');
	iframe.setAttribute('src', 'https://www.youtube.com/embed/' + event.currentTarget.dataset.id + '?autoplay=1');
	iframe.setAttribute('allow', 'accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture');
	iframe.setAttribute('allowfullscreen', '');
	iframe.setAttribute('frameborder', '0');

	Site.is_mobile() ? iframe.setAttribute('style','width: 80%; height: auto') : iframe.setAttribute('style','width: 360px; height: 270px');

	event.currentTarget.classList.add('video_showing');
	event.currentTarget.replaceChild(iframe, event.target);
}

/**
 * Function called when document and images have been completely loaded.
 */
Site.on_load = function() {

	if (Site.is_mobile())
		Site.mobile_menu = new Caracal.MobileMenu();

	// create new page control
	if (window.location.pathname == '/') {
 		Caracal.slides = new PageControl('section.header', 'div.slide');

		if (Site.is_mobile()) {
			Caracal.slides
	 				.setWrapAround(true)
	 				.setPauseOnHover(true)
	 				.attachNextControl($('a.next'))
	 				.attachPreviousControl($('a.previous'))
					.attachControls('div#controls a');
					 
			// toggle banner visibility on home page
			var banner = document.getElementById('banner');
			banner.addEventListener('touchend', function(event) {
				var element = event.target;
				element.classList.contains('shown') ? element.classList.remove('shown') : element.classList.add('shown');
			});
		}

	 	if(!Site.is_mobile()) {
		 	Caracal.slides
		 				.setInterval(10000)
		 				.setWrapAround(true)
		 				.setPauseOnHover(true)
		 				.attachNextControl($('a.next'))
		 				.attachPreviousControl($('a.previous'))
		 				.attachControls('div#controls a');

		 	Caracal.lightbox = new LightBox('section.gallery div.images a.image', false, false, true);
		}
	}

	if(window.location.pathname == "/gallery")
		Caracal.lightbox = new LightBox('section.gallery div.images a.image', false, false, true);

	var thumbnails = document.querySelectorAll('a.thumbnail');
	thumbnails.forEach(function(thumbnail) {
		thumbnail.addEventListener('click', Site.handleVideoOpen);
	});
};


// connect document `load` event with handler function
$(Site.on_load);
